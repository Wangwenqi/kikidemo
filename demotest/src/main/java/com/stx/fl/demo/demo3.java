package com.stx.fl.demo;


import org.junit.Test;
import org.springframework.test.context.ContextConfiguration;
import javax.annotation.Resource;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class demo3 {
    @Resource(name = "student")
    Student student ;

    @Test
    public void testUser(){

        ApplicationContext applicationContext = new ClassPathXmlApplicationContext("applicationContext.xml");

        System.out.println("----"+student);

        student.save();
        student.update();
        student.delete();

    }


}
