package com.stx.fl.demo;

public interface Student {
    public void save();
    public void update();
    public void delete();
}
