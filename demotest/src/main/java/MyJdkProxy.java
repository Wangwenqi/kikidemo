import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;
import java.lang.reflect.Proxy;
import java.util.Map;

public class MyJdkProxy implements InvocationHandler{

    private UserDao userDao;

    public MyJdkProxy(UserDao userDao){

        this.userDao = userDao;
    }
    public Object createProxy(){

        return  Proxy.newProxyInstance(userDao.getClass().getClassLoader(),userDao.getClass().getInterfaces(),this);

    }

    public Object invoke(Object proxy, Method method, Object[] args) throws Throwable {

        if (method.getName().equals("add")){

            System.out.println("～～～！校验权限！～xxxx哈哈哈哈xx～sssssss=～～");
            return method.invoke(userDao,args);

        }
        return method.invoke(userDao,args);
    }
}
