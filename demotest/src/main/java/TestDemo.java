import org.junit.Test;

public class TestDemo {


    @Test
    public void testUser(){

        UserDao user = new UserDaoImpl();

        UserDao proxy = (UserDao) new MyJdkProxy(user).createProxy();
        proxy.add();
        proxy.update();
        proxy.select();

    }

}
